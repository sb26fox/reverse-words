﻿using System.Net.Sockets;
using NUnit.Framework;
using Moq;
using WordReverserLib;

namespace WordReverserTests
{
    [TestFixture]
    public class WordReverserShould
    {
        [Test]
        public void ReturnsTrue_IfFile_IsExists()
        {
            var reverser = Mock.Of<IWordReverser>(x => x.IsExists(@"D:\testFile.txt") == true);
            var stub = new StubWordReverser(reverser);

            bool result = stub.Reverser.IsExists(@"D:\testFile.txt");

            Assert.IsTrue(result);
        }

        [Test]
        public void ReturnsDataFromFile_IfFile_IsExists()
        {
            string[] content = {"line1", "line2"};
            var reverser = Mock.Of<IWordReverser>(x => x.ReadDataFromFile(@"D:\testFile.txt") == content);
            var stub = new StubWordReverser(reverser);

            var result = stub.Reverser.ReadDataFromFile(@"D:\testFile.txt");
            Assert.AreEqual(content, result);
        }

        [Test]
        public void ReversesWords_IfInput_IsWords()
        {
            var reverser = new WordReverser();
            var result = reverser.Reverse("Hello World");
            Assert.AreEqual("World Hello", result);
        }

        [Test]
        public void ReverseWordsFromFile_IfFile_IsExists()
        {
            string[] resultExpected = { "World Hello", "Eval Code" };
            var reverser = Mock.Of<IWordReverser>(x => x.ReverseData(@"D:\testFile.txt") == resultExpected);
            var stub = new StubWordReverser(reverser);


            var result = stub.Reverser.ReverseData(@"D:\testFile.txt");
            Assert.AreEqual(resultExpected, result);
        }
    }
}