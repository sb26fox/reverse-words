﻿using System.Collections.Generic;
using System.IO;

namespace WordReverserLib
{
    public interface IWordReverser
    {
        bool IsExists(string path);
        string[] ReadDataFromFile(string path);
        string[] ReverseData(string path);
    }

    public class WordReverser : IWordReverser
    {
        public bool IsExists(string path)
        {
            return File.Exists(path);
        }

        public string[] ReadDataFromFile(string path)
        {
            if (IsExists(path))
            {
                return File.ReadAllLines(path);
            }
            else
            {
                throw new FileNotFoundException(string.Format("Файла {0} не существует!", path));
            }
        }

        public string Reverse(string input)
        {
            var words = input.Split(' ');

            return words[1] + " " + words[0];
        }

        public string[] ReverseData(string path)
        {
            var destData = ReadDataFromFile(path);
            var reversedData = new List<string>();


            foreach (var line in destData)
            {
                reversedData.Add(Reverse(line));
            }

            return reversedData.ToArray();
        }
    }

    public class StubWordReverser
    {
        public IWordReverser Reverser;

        public StubWordReverser()
        {
            Reverser = new WordReverser();
        }

        public StubWordReverser(IWordReverser reverser)
        {
            Reverser = reverser;
        }
    }
}